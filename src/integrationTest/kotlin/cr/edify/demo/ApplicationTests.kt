package cr.edify.demo

import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class ApplicationTests : BaseIntegrationTest() {

    @Test
    @DisplayName("Spring context should load")
    fun contextLoads() = Unit
}
