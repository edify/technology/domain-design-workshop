package cr.edify.demo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.devtools.restart.RestartScope
import org.springframework.boot.fromApplication
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.context.annotation.Bean
import org.springframework.test.context.DynamicPropertyRegistry
import org.testcontainers.containers.BindMode
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.localstack.LocalStackContainer
import org.testcontainers.utility.DockerImageName

@SpringBootApplication
@TestConfiguration(proxyBeanMethods = false)
class TestApplication {

    @Bean
    fun dockerNetwork(): Network {
        return Network.newNetwork()
    }

    @Bean
    @RestartScope
    @ServiceConnection
    fun postgresContainer(network: Network): PostgreSQLContainer<*> {
        return PostgreSQLContainer(DockerImageName.parse("postgres:15.1"))
            .withUsername("demo")
            .withPassword("demo")
            .withDatabaseName("demo")
            .withNetwork(network)
            .withNetworkAliases("postgres")
            .withReuse(true)
    }

    @Bean
    fun localStackContainer(network: Network, propertyRegistry: DynamicPropertyRegistry): GenericContainer<*> {
        val dockerImageName = DockerImageName.parse("localstack/localstack:2.2.0")
        val container = LocalStackContainer(dockerImageName)
            .withFileSystemBind(
                "./containers/localstack",
                "/etc/localstack/init/ready.d",
                BindMode.READ_ONLY
            )
            .withServices(LocalStackContainer.Service.SQS)
            .withNetworkAliases("localstack")
            .withNetwork(network)

        propertyRegistry.add("spring.cloud.aws.sqs.region", container::getRegion)
        propertyRegistry.add("spring.cloud.aws.sqs.endpoint", container::getEndpoint)
        propertyRegistry.add("spring.cloud.aws.credentials.access-key", container::getAccessKey)
        propertyRegistry.add("spring.cloud.aws.credentials.secret-key", container::getSecretKey)

        return container
    }

    @Bean
    @ServiceConnection(name = "otel/opentelemetry-collector-contrib")
    fun otelCollector(network: Network, propertyRegistry: DynamicPropertyRegistry): GenericContainer<*> {
        val dockerImageName = DockerImageName.parse("otel/opentelemetry-collector-contrib:0.89.0")

        return GenericContainer(dockerImageName)
            .withFileSystemBind(
                "./containers/telemetry/otel-collector/config.yml",
                "/etc/otelcol-contrib/config.yaml",
                BindMode.READ_ONLY
            )
            .withExposedPorts(4317, 4318)
            .withNetwork(network)
            .withNetworkAliases("otel-collector")
    }

    @Bean
    fun jaegertracingContainer(network: Network): GenericContainer<*> {
        val dockerImageName = DockerImageName.parse("jaegertracing/all-in-one:1.50.0")

        return GenericContainer(dockerImageName)
            .withExposedPorts(4317, 16686)
            .withNetwork(network)
            .withNetworkAliases("jaeger")
    }

    @Bean
    fun prometheusContainer(network: Network): GenericContainer<*> {
        val dockerImageName = DockerImageName.parse("prom/prometheus:v2.47.2")

        return GenericContainer(dockerImageName)
            .withFileSystemBind(
                "./containers/telemetry/prometheus/config.yml",
                "/etc/prometheus/prometheus.yml",
                BindMode.READ_ONLY
            )
            .withExposedPorts(9090)
            .withNetwork(network)
            .withNetworkAliases("prometheus")
    }

    @Bean
    fun grafanaContainer(network: Network): GenericContainer<*> {
        val dockerImageName = DockerImageName.parse("grafana/grafana:10.2.2")

        return GenericContainer(dockerImageName)
            .withExposedPorts(3000)
            .withNetwork(network)
            .withNetworkAliases("grafana")
    }
}

fun main(args: Array<String>) {
    fromApplication<Application>().with(TestApplication::class.java).run(*args)
}
