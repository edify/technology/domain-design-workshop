package cr.edify.demo

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.BindMode
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.localstack.LocalStackContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Testcontainers
@DirtiesContext
class BaseIntegrationTest {

    companion object {
        @Container
        @ServiceConnection
        val postgresqlContainer = PostgreSQLContainer(DockerImageName.parse("postgres:15.1"))

        @Container
        val localStackContainer: LocalStackContainer =
            LocalStackContainer(DockerImageName.parse("localstack/localstack:2.2.0"))
                .withClasspathResourceMapping(
                    "/localstack",
                    "/etc/localstack/init/ready.d",
                    BindMode.READ_ONLY
                ).withServices(LocalStackContainer.Service.SQS)

        @JvmStatic
        @DynamicPropertySource
        fun dynamicProps(propertyRegistry: DynamicPropertyRegistry) {
            propertyRegistry.add("spring.cloud.aws.sqs.region", localStackContainer::getRegion)
            propertyRegistry.add("spring.cloud.aws.sqs.endpoint", localStackContainer::getEndpoint)
            propertyRegistry.add("spring.cloud.aws.credentials.access-key", localStackContainer::getAccessKey)
            propertyRegistry.add("spring.cloud.aws.credentials.secret-key", localStackContainer::getSecretKey)
        }
    }
}
