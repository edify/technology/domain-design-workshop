package cr.edify.demo.notifications

import com.github.f4b6a3.uuid.UuidCreator
import cr.edify.demo.common.valuetypes.ExternalId
import cr.edify.demo.common.valuetypes.Id
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.time.Duration
import java.time.Instant
import java.util.*
import java.util.stream.Stream

@DisplayName("No goals enrolled notification")
class NoGoalsEnrolledNotificationTest {
    companion object {
        @JvmStatic
        fun notificationProvider(): Stream<Arguments> {
            val configuration = NoGoalsEnrolledNotificationConfiguration(
                Id(UuidCreator.getTimeOrderedEpoch()),
                Duration.ofMinutes(5),
                ExternalId(1)
            )

            return Stream.of(
                Arguments.of(
                    NoGoalsEnrolledNotification(
                        Id(UuidCreator.getTimeOrderedEpoch()),
                        ExternalId(1),
                        ExternalId(1),
                        configuration,
                        Instant.now()
                    ),
                    NotReady,
                    "on a newly created notification the status should be NOT_READY"
                ),
                Arguments.of(
                    NoGoalsEnrolledNotification(
                        Id(UuidCreator.getTimeOrderedEpoch()),
                        ExternalId(1),
                        ExternalId(1),
                        configuration,
                        Instant.now().minus(Duration.ofMinutes(5))
                    ),
                    Ready,
                    "on a notification that meets the trigger threshold exactly the status should be READY"
                ),
                Arguments.of(
                    NoGoalsEnrolledNotification(
                        Id(UuidCreator.getTimeOrderedEpoch()),
                        ExternalId(1),
                        ExternalId(1),
                        configuration,
                        Instant.now().minus(Duration.ofMinutes(6))
                    ),
                    Ready,
                    "on a notification that is past the trigger threshold the status should be READY"
                ),
                Arguments.of(
                    NoGoalsEnrolledNotification(
                        Id(UuidCreator.getTimeOrderedEpoch()),
                        ExternalId(1),
                        ExternalId(1),
                        configuration,
                        Instant.now(),
                        Optional.of(Instant.now().plus(Duration.ofMinutes(1)))
                    ),
                    Obsolete,
                    "on a notification that can't meet the trigger threshold the status should be OBSOLETE"
                )
            )
        }
    }

    @ParameterizedTest(name = "{2}")
    @MethodSource("cr.edify.demo.notifications.NoGoalsEnrolledNotificationTest#notificationProvider")
    @DisplayName("when calling doNotify")
    fun testDoNotify(notification: NoGoalsEnrolledNotification, status: NotificationStatus, displayName: String) {
        notification.doNotify()

        assertThat(notification.status).isEqualTo(status)
    }
}
