package cr.edify.demo.events

import cr.edify.demo.common.serializers.InstantSerializer
import cr.edify.demo.common.valuetypes.ExternalId
import kotlinx.serialization.Serializable
import java.time.Instant

@Serializable
data class StudentAccountCreatedEvent(
    @Serializable(with = InstantSerializer::class)
    override val occurredAt: Instant,
    override val customerId: ExternalId,
    val studentId: ExternalId
) : ExternalEvent()
