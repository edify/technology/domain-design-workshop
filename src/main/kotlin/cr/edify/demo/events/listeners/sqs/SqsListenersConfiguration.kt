package cr.edify.demo.events.listeners.sqs

import cr.edify.demo.events.GoalEnrollmentEvent
import cr.edify.demo.events.StudentAccountCreatedEvent
import cr.edify.demo.events.listeners.GoalEnrollmentEventListener
import cr.edify.demo.events.listeners.StudentAccountCreatedEventListener
import cr.edify.demo.notifications.services.NotificationService
import io.awspring.cloud.sqs.listener.MessageListenerContainer
import io.awspring.cloud.sqs.listener.SqsMessageListenerContainer
import io.micrometer.observation.ObservationRegistry
import io.micrometer.tracing.propagation.Propagator
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.services.sqs.SqsAsyncClient

@Configuration
class SqsListenersConfiguration {
    @Bean
    fun studentAccountCreatedSqsListenerContainer(
        propagator: Propagator,
        observationRegistry: ObservationRegistry,
        sqsAsyncClient: SqsAsyncClient,
        notificationService: NotificationService,
        @Value("\${demo.queues.student-account-created-event}") queueName: String
    ): MessageListenerContainer<String> {
        return SqsMessageListenerContainer.builder<String>()
            .id("account-created")
            .sqsAsyncClient(sqsAsyncClient)
            .messageListener(
                ObservableSqsMessageListener(
                    observationRegistry,
                    "listener.student-account-created-event",
                    "student-account-created-event-listener",
                    StudentAccountCreatedEventListener(notificationService),
                    StudentAccountCreatedEvent.serializer()
                )
            ).queueNames(queueName).build()
    }

    @Bean
    fun goalEnrollmentEventSqsListenerContainer(
        observationRegistry: ObservationRegistry,
        sqsAsyncClient: SqsAsyncClient,
        notificationService: NotificationService,
        @Value("\${demo.queues.goal-enrollment-event}") queueName: String
    ): MessageListenerContainer<String> {
        return SqsMessageListenerContainer.builder<String>()
            .id("goal-enrolled")
            .sqsAsyncClient(sqsAsyncClient)
            .messageListener(
                ObservableSqsMessageListener(
                    observationRegistry,
                    "listener.goal-enrollment-event",
                    "goal-enrollment-event-listener",
                    GoalEnrollmentEventListener(notificationService),
                    GoalEnrollmentEvent.serializer()
                )
            ).queueNames(queueName).build()
    }
}
