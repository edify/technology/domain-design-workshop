package cr.edify.demo.events.listeners

import cr.edify.demo.events.GoalEnrollmentEvent
import cr.edify.demo.notifications.services.NotificationService
import io.github.oshai.kotlinlogging.KotlinLogging

private val logger = KotlinLogging.logger {}

class GoalEnrollmentEventListener(private val notificationService: NotificationService) :
    ExternalEventListener<GoalEnrollmentEvent> {

    override fun listen(event: GoalEnrollmentEvent) {
        logger.debug { "Processing goal enrollment event [$event]" }
        notificationService.updateNoGoalsEnrollmentNotification(event.studentId, event.customerId, event.occurredAt)
    }
}
