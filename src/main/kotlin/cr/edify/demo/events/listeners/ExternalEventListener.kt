package cr.edify.demo.events.listeners

interface ExternalEventListener<T> {
    fun listen(event: T)
}
