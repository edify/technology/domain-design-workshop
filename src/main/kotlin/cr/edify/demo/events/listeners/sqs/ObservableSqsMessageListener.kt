package cr.edify.demo.events.listeners.sqs

import cr.edify.demo.events.listeners.ExternalEventListener
import io.awspring.cloud.sqs.listener.MessageListener
import io.micrometer.observation.Observation
import io.micrometer.observation.ObservationRegistry
import io.micrometer.observation.transport.Kind
import io.micrometer.observation.transport.ReceiverContext
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.json.Json
import org.springframework.messaging.Message
import org.springframework.messaging.MessageHeaders

class ObservableSqsMessageListener<T>(
    private val observationRegistry: ObservationRegistry,
    private val observationName: String,
    private val observationContextualName: String,
    private val externalEventListener: ExternalEventListener<T>,
    private val deserializer: DeserializationStrategy<T>
) :
    MessageListener<String> {
    override fun onMessage(message: Message<String>) {
        val receiverContext =
            ReceiverContext<MessageHeaders>({ carrier, key -> carrier[key] as String? }, Kind.CONSUMER).also {
                it.carrier = message.headers
            }
        Observation.start(observationName, { receiverContext }, observationRegistry)
            .contextualName(observationContextualName)
            .observe {
                externalEventListener.listen(Json.decodeFromString(deserializer, message.payload))
            }
    }
}
