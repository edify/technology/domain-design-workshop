package cr.edify.demo.events.listeners

import cr.edify.demo.events.StudentAccountCreatedEvent
import cr.edify.demo.notifications.services.NotificationService
import io.github.oshai.kotlinlogging.KotlinLogging

private val logger = KotlinLogging.logger {}

class StudentAccountCreatedEventListener(
    private val notificationService: NotificationService
) : ExternalEventListener<StudentAccountCreatedEvent> {

    override fun listen(event: StudentAccountCreatedEvent) {
        logger.debug { "Processing account created event [$event]" }
        notificationService.createNoGoalsEnrolledNotification(event.studentId, event.customerId, event.occurredAt)
    }
}
