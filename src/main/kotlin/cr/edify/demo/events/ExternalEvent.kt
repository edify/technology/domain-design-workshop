package cr.edify.demo.events

import cr.edify.demo.common.valuetypes.ExternalId
import java.time.Instant

sealed class ExternalEvent {
    abstract val occurredAt: Instant
    abstract val customerId: ExternalId
}
