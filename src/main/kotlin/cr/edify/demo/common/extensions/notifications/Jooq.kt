package cr.edify.demo.common.extensions.notifications

import cr.edify.demo.jooq.tables.records.NoGoalsEnrolledNotificationsRecord
import cr.edify.demo.notifications.NoGoalsEnrolledNotification
import java.time.OffsetDateTime
import java.time.ZoneOffset

fun NoGoalsEnrolledNotification.toJooqRecord() = NoGoalsEnrolledNotificationsRecord(
    this.id.id,
    this.customerId.id,
    this.studentId.id,
    this.status,
    OffsetDateTime.ofInstant(this.accountCreatedAt, ZoneOffset.UTC),
    this.goalEnrolledAt.map { it.atOffset(ZoneOffset.UTC) }.orElse(null),
    this.configuration.id.id
)
