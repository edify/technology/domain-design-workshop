package cr.edify.demo.common.jooq.mappers

import cr.edify.demo.common.valuetypes.ExternalId
import cr.edify.demo.common.valuetypes.Id
import cr.edify.demo.jooq.tables.records.NoGoalsEnrolledNotificationConfigurationsRecord
import cr.edify.demo.jooq.tables.records.NoGoalsEnrolledNotificationsRecord
import cr.edify.demo.notifications.NoGoalsEnrolledNotification
import org.jooq.Record2
import org.jooq.RecordMapper
import java.util.*

typealias NoGoalsEnrolledNotificationWithConfigurationRecord =
        Record2<NoGoalsEnrolledNotificationsRecord, NoGoalsEnrolledNotificationConfigurationsRecord>

class NoGoalsEnrolledNotificationMapper :
    RecordMapper<NoGoalsEnrolledNotificationWithConfigurationRecord, NoGoalsEnrolledNotification> {

    private val noGoalsEnrolledNotificationConfigurationMapper = NoGoalsEnrolledNotificationConfigurationMapper()

    override fun map(record: NoGoalsEnrolledNotificationWithConfigurationRecord): NoGoalsEnrolledNotification {
        val notificationRecord = record.component1()
        val notificationConfigurationRecord = record.component2()

        val goalEnrolledAt = Optional.ofNullable(notificationRecord.goalEnrolledAt).map { it.toInstant() }

        return NoGoalsEnrolledNotification(
            Id(notificationRecord.id),
            ExternalId(notificationRecord.customerId),
            ExternalId(notificationRecord.studentId),
            noGoalsEnrolledNotificationConfigurationMapper.map(notificationConfigurationRecord),
            notificationRecord.accountCreatedAt.toInstant(),
            goalEnrolledAt,
            notificationRecord.status
        )
    }
}

