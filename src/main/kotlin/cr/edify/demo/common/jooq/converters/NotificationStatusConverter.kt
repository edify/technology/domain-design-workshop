package cr.edify.demo.common.jooq.converters

import cr.edify.demo.notifications.NotReady
import cr.edify.demo.notifications.NotificationStatus
import cr.edify.demo.notifications.Notify
import cr.edify.demo.notifications.Obsolete
import cr.edify.demo.notifications.Ready
import org.jooq.impl.AbstractConverter

class NotificationStatusConverter :
    AbstractConverter<cr.edify.demo.jooq.enums.NotificationStatus, NotificationStatus>(
        cr.edify.demo.jooq.enums.NotificationStatus::class.java,
        NotificationStatus::class.java
    ) {
    override fun from(databaseObject: cr.edify.demo.jooq.enums.NotificationStatus) = when (databaseObject) {
        cr.edify.demo.jooq.enums.NotificationStatus.NOTIFY -> Notify
        cr.edify.demo.jooq.enums.NotificationStatus.READY -> Ready
        cr.edify.demo.jooq.enums.NotificationStatus.NOT_READY -> NotReady
        cr.edify.demo.jooq.enums.NotificationStatus.OBSOLETE -> Obsolete
    }

    override fun to(userObject: NotificationStatus) =
        cr.edify.demo.jooq.enums.NotificationStatus.valueOf(userObject.value())
}
