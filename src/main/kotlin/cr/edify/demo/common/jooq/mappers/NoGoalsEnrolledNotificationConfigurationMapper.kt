package cr.edify.demo.common.jooq.mappers

import cr.edify.demo.common.valuetypes.ExternalId
import cr.edify.demo.common.valuetypes.Id
import cr.edify.demo.jooq.tables.records.NoGoalsEnrolledNotificationConfigurationsRecord
import cr.edify.demo.notifications.NoGoalsEnrolledNotificationConfiguration
import org.jooq.RecordMapper

class NoGoalsEnrolledNotificationConfigurationMapper
    : RecordMapper<NoGoalsEnrolledNotificationConfigurationsRecord, NoGoalsEnrolledNotificationConfiguration> {
    override fun map(record: NoGoalsEnrolledNotificationConfigurationsRecord)
            : NoGoalsEnrolledNotificationConfiguration {

        return NoGoalsEnrolledNotificationConfiguration(
            Id(record.id),
            record.timeBetweenEvents.toDuration(),
            ExternalId(record.customerId)
        )
    }
}
