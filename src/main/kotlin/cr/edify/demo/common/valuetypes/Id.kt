package cr.edify.demo.common.valuetypes

import java.util.*

@JvmInline
value class Id(val id: UUID)
