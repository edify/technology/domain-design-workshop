package cr.edify.demo.common.valuetypes

import kotlinx.serialization.Serializable

@JvmInline
@Serializable
value class ExternalId(val id: Long)
