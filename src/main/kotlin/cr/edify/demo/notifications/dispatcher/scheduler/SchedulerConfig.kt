package cr.edify.demo.notifications.dispatcher.scheduler

import cr.edify.demo.notifications.dispatcher.NotificationDispatcherTask
import cr.edify.demo.notifications.dispatcher.NotificationDispatcherTaskImpl
import cr.edify.demo.notifications.services.NotificationService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.SchedulingConfigurer
import org.springframework.scheduling.config.ScheduledTaskRegistrar
import java.time.Duration

@Configuration
class SchedulerConfig(private val notificationService: NotificationService) : SchedulingConfigurer {
    @Bean
    fun notificationDispatcherTask(): NotificationDispatcherTask {
        return NotificationDispatcherTaskImpl(notificationService)
    }

    override fun configureTasks(taskRegistrar: ScheduledTaskRegistrar) {
        taskRegistrar.addFixedRateTask(notificationDispatcherTask(), Duration.ofMinutes(1))
    }
}
