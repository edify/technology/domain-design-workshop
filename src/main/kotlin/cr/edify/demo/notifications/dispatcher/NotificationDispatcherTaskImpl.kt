package cr.edify.demo.notifications.dispatcher

import cr.edify.demo.notifications.services.NotificationService
import io.micrometer.observation.annotation.Observed

open class NotificationDispatcherTaskImpl(private val notificationService: NotificationService) :
    NotificationDispatcherTask {
    @Observed(name = "scheduled.dispatch-notifications", contextualName = "dispatch-notifications")
    override fun run() {
        notificationService.dispatchNotifications()
    }
}
