package cr.edify.demo.notifications.services

import cr.edify.demo.common.valuetypes.ExternalId
import org.springframework.stereotype.Service
import java.time.Instant

@Service
interface NotificationService {
    fun createNoGoalsEnrolledNotification(studentId: ExternalId, customerId: ExternalId, accountCreatedAt: Instant)
    fun updateNoGoalsEnrollmentNotification(studentId: ExternalId, customerId: ExternalId, goalEnrolledAt: Instant)
    fun dispatchNotifications()
}
