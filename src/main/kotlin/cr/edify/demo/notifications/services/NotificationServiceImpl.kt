package cr.edify.demo.notifications.services

import com.github.f4b6a3.uuid.UuidCreator
import cr.edify.demo.common.valuetypes.ExternalId
import cr.edify.demo.common.valuetypes.Id
import cr.edify.demo.notifications.NoGoalsEnrolledNotification
import cr.edify.demo.notifications.repositories.NotificationConfigurationRepository
import cr.edify.demo.notifications.repositories.NotificationRepository
import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

private val logger = KotlinLogging.logger {}

@Service
class NotificationServiceImpl(
    private val notificationRepository: NotificationRepository,
    private val notificationConfigurationRepository: NotificationConfigurationRepository
) : NotificationService {
    override fun createNoGoalsEnrolledNotification(
        studentId: ExternalId,
        customerId: ExternalId,
        accountCreatedAt: Instant
    ) {
        val notificationConfiguration =
            notificationConfigurationRepository.findNoGoalsEnrolledNotificationConfigurationByCustomerId(customerId)

        notificationRepository.create(
            NoGoalsEnrolledNotification(
                id = Id(UuidCreator.getTimeOrderedEpoch()),
                customerId = customerId,
                studentId = studentId,
                configuration = notificationConfiguration.orElseThrow(),
                accountCreatedAt = accountCreatedAt
            )
        )
    }

    override fun updateNoGoalsEnrollmentNotification(
        studentId: ExternalId,
        customerId: ExternalId,
        goalEnrolledAt: Instant
    ) {
        val notification = notificationRepository.findNoGoalsEnrollmentNotificationByStudentAndCustomer(
            studentId,
            customerId
        ).orElseThrow()

        notification.goalEnrolledAt = Optional.of(goalEnrolledAt)
        notificationRepository.update(notification)
    }

    @Transactional
    override fun dispatchNotifications() {
        val notifiableNotifications = notificationRepository.findNotifiableNotifications()

        logger.debug { "Processing [${notifiableNotifications.size}] notifications..." }

        for (notification in notifiableNotifications) {
            notification.doNotify()
            notificationRepository.update(notification)
        }
    }
}
