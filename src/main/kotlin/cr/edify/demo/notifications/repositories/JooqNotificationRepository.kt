package cr.edify.demo.notifications.repositories

import cr.edify.demo.common.extensions.notifications.toJooqRecord
import cr.edify.demo.common.jooq.mappers.NoGoalsEnrolledNotificationMapper
import cr.edify.demo.common.valuetypes.ExternalId
import cr.edify.demo.jooq.tables.references.NO_GOALS_ENROLLED_NOTIFICATIONS
import cr.edify.demo.jooq.tables.references.NO_GOALS_ENROLLED_NOTIFICATION_CONFIGURATIONS
import cr.edify.demo.notifications.NoGoalsEnrolledNotification
import cr.edify.demo.notifications.NotReady
import cr.edify.demo.notifications.Notification
import org.jooq.DSLContext
import org.springframework.stereotype.Repository
import java.time.ZoneOffset
import java.util.*

@Repository
class JooqNotificationRepository(private val jooq: DSLContext) : NotificationRepository {
    override fun create(notification: Notification) {
        when (notification) {
            is NoGoalsEnrolledNotification -> create(notification)
        }
    }

    override fun findNoGoalsEnrollmentNotificationByStudentAndCustomer(
        studentId: ExternalId,
        customerId: ExternalId
    ): Optional<NoGoalsEnrolledNotification> {
        return jooq.select(NO_GOALS_ENROLLED_NOTIFICATIONS, NO_GOALS_ENROLLED_NOTIFICATION_CONFIGURATIONS)
            .from(NO_GOALS_ENROLLED_NOTIFICATIONS)
            .join(NO_GOALS_ENROLLED_NOTIFICATION_CONFIGURATIONS)
            .on(
                NO_GOALS_ENROLLED_NOTIFICATION_CONFIGURATIONS.CUSTOMER_ID.eq(
                    NO_GOALS_ENROLLED_NOTIFICATIONS.CUSTOMER_ID
                )
            ).where(NO_GOALS_ENROLLED_NOTIFICATIONS.STUDENT_ID.eq(studentId.id))
            .and(NO_GOALS_ENROLLED_NOTIFICATIONS.CUSTOMER_ID.eq(customerId.id))
            .fetchOptional(NoGoalsEnrolledNotificationMapper())
    }

    override fun update(notification: Notification) {
        when (notification) {
            is NoGoalsEnrolledNotification -> update(notification)
        }
    }

    override fun findNotifiableNotifications(): List<Notification> {
        return findNotifiableNoGoalsEnrolledNotifications()
    }

    private fun findNotifiableNoGoalsEnrolledNotifications(): List<NoGoalsEnrolledNotification> {
        return jooq.select(NO_GOALS_ENROLLED_NOTIFICATIONS, NO_GOALS_ENROLLED_NOTIFICATION_CONFIGURATIONS)
            .from(NO_GOALS_ENROLLED_NOTIFICATIONS)
            .join(NO_GOALS_ENROLLED_NOTIFICATION_CONFIGURATIONS)
            .on(NO_GOALS_ENROLLED_NOTIFICATION_CONFIGURATIONS.CUSTOMER_ID.eq(NO_GOALS_ENROLLED_NOTIFICATIONS.CUSTOMER_ID))
            .where(NO_GOALS_ENROLLED_NOTIFICATIONS.STATUS.eq(NotReady))
            .fetch(NoGoalsEnrolledNotificationMapper())
    }

    private fun create(notification: NoGoalsEnrolledNotification) {
        jooq.insertInto(NO_GOALS_ENROLLED_NOTIFICATIONS).set(notification.toJooqRecord()).execute()
    }

    private fun update(notification: NoGoalsEnrolledNotification) {
        jooq.update(NO_GOALS_ENROLLED_NOTIFICATIONS)
            .set(NO_GOALS_ENROLLED_NOTIFICATIONS.STATUS, notification.status)
            .set(
                NO_GOALS_ENROLLED_NOTIFICATIONS.GOAL_ENROLLED_AT,
                notification.goalEnrolledAt.map { it.atOffset(ZoneOffset.UTC) }.orElse(null)
            ).where(NO_GOALS_ENROLLED_NOTIFICATIONS.ID.eq(notification.id.id))
            .execute()
    }
}
