package cr.edify.demo.notifications.repositories

import cr.edify.demo.common.valuetypes.ExternalId
import cr.edify.demo.notifications.NoGoalsEnrolledNotificationConfiguration
import java.util.*

interface NotificationConfigurationRepository {
    fun findNoGoalsEnrolledNotificationConfigurationByCustomerId(customerId: ExternalId): Optional<NoGoalsEnrolledNotificationConfiguration>
}
