package cr.edify.demo.notifications.repositories

import cr.edify.demo.common.valuetypes.ExternalId
import cr.edify.demo.notifications.NoGoalsEnrolledNotification
import cr.edify.demo.notifications.Notification
import java.util.*

interface NotificationRepository {
    fun create(notification: Notification)
    fun findNoGoalsEnrollmentNotificationByStudentAndCustomer(
        studentId: ExternalId,
        customerId: ExternalId
    ): Optional<NoGoalsEnrolledNotification>

    fun update(notification: Notification)
    fun findNotifiableNotifications(): List<Notification>
}
