package cr.edify.demo.notifications.repositories

import cr.edify.demo.common.jooq.mappers.NoGoalsEnrolledNotificationConfigurationMapper
import cr.edify.demo.common.valuetypes.ExternalId
import cr.edify.demo.jooq.tables.references.NO_GOALS_ENROLLED_NOTIFICATION_CONFIGURATIONS
import cr.edify.demo.notifications.NoGoalsEnrolledNotificationConfiguration
import org.jooq.DSLContext
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class JooqNotificationConfigurationRepository(private val jooq: DSLContext) : NotificationConfigurationRepository {
    override fun findNoGoalsEnrolledNotificationConfigurationByCustomerId(customerId: ExternalId): Optional<NoGoalsEnrolledNotificationConfiguration> {
        return jooq.selectFrom(NO_GOALS_ENROLLED_NOTIFICATION_CONFIGURATIONS)
            .where(NO_GOALS_ENROLLED_NOTIFICATION_CONFIGURATIONS.CUSTOMER_ID.eq(customerId.id))
            .fetchOptional(NoGoalsEnrolledNotificationConfigurationMapper())
    }
}
