package cr.edify.demo.notifications

import cr.edify.demo.common.valuetypes.ExternalId
import cr.edify.demo.common.valuetypes.Id
import java.time.Duration
import java.time.Instant
import java.util.*

data class NoGoalsEnrolledNotification(
    override val id: Id,
    override val customerId: ExternalId,
    val studentId: ExternalId,
    val configuration: NoGoalsEnrolledNotificationConfiguration,
    val accountCreatedAt: Instant,
    var goalEnrolledAt: Optional<Instant> = Optional.empty(),
    override var status: NotificationStatus = NotReady,
) : Notification() {
    override fun notificationThresholdReached(): Boolean {
        return Duration.between(accountCreatedAt, Instant.now()) >= configuration.timeBetweenEvents
    }

    override fun notificationObsolescenceReached(): Boolean {
        return goalEnrolledAt.map {
            Duration.between(accountCreatedAt, it) < configuration.timeBetweenEvents
        }.orElse(false)
    }
}
