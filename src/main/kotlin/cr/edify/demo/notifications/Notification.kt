package cr.edify.demo.notifications

import cr.edify.demo.common.valuetypes.ExternalId
import cr.edify.demo.common.valuetypes.Id

sealed class Notification {
    abstract val id: Id
    abstract val customerId: ExternalId
    abstract var status: NotificationStatus

    abstract fun notificationThresholdReached(): Boolean
    abstract fun notificationObsolescenceReached(): Boolean

    fun doNotify() {
        this.status = if (notificationObsolescenceReached()) {
            Obsolete
        } else if (notificationThresholdReached()) {
            Ready
        } else {
            NotReady
        }
    }
}

