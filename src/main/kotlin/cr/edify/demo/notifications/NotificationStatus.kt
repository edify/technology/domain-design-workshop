package cr.edify.demo.notifications

import net.pearx.kasechange.toScreamingSnakeCase

sealed class NotificationStatus {
    fun value() = this.javaClass.simpleName.toScreamingSnakeCase()
}

data object Notify : NotificationStatus()

data object NotReady : NotificationStatus()

data object Ready : NotificationStatus()

data object Obsolete : NotificationStatus()
