package cr.edify.demo.notifications

import cr.edify.demo.common.valuetypes.ExternalId
import cr.edify.demo.common.valuetypes.Id
import java.time.Duration

data class NoGoalsEnrolledNotificationConfiguration(
    val id: Id,
    val timeBetweenEvents: Duration,
    val customerId: ExternalId
)
