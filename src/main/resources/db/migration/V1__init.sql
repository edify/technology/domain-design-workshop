CREATE TYPE NOTIFICATION_STATUS AS ENUM ('READY', 'NOT_READY', 'OBSOLETE', 'NOTIFY');

CREATE TABLE no_goals_enrolled_notification_configurations
(
    id                  UUID PRIMARY KEY,
    customer_id         BIGINT   NOT NULL,
    time_between_events INTERVAL NOT NULL
);

CREATE INDEX idx_no_goals_enrolled_notification_configurations_customer_id ON no_goals_enrolled_notification_configurations (customer_id);

CREATE TABLE no_goals_enrolled_notifications
(
    id                 UUID PRIMARY KEY,
    customer_id        BIGINT                   NOT NULL,
    student_id         BIGINT                   NOT NULL,
    status             NOTIFICATION_STATUS      NOT NULL,
    account_created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    goal_enrolled_at   TIMESTAMP WITH TIME ZONE,
    configuration_id   UUID                     NOT NULL REFERENCES no_goals_enrolled_notification_configurations (id)
);

CREATE INDEX idx_no_goals_enrolled_notifications_status ON no_goals_enrolled_notifications (status);
CREATE INDEX idx_no_goals_enrolled_notifications_customer_id_student_id ON no_goals_enrolled_notifications (customer_id, student_id);
