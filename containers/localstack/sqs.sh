#!/usr/bin/env bash

set -euo pipefail

echo "==================="
echo "| Configuring SQS |"
echo "==================="

get_all_queues() {
	awslocal sqs list-queues
}

create_queue() {
	local QUEUE_NAME_TO_CREATE=$1
	awslocal sqs create-queue --queue-name "${QUEUE_NAME_TO_CREATE}"
}

for queue in demo-goal-enrolled demo-student-account-created; do
	echo "Creating queue ${queue}..."
	create_queue $queue
done

echo "Created queues"

get_all_queues
