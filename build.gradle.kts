import org.gradle.api.tasks.PathSensitivity.RELATIVE
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jooq.meta.jaxb.ForcedType
import org.testcontainers.containers.PostgreSQLContainer

buildscript {
    repositories { mavenCentral() }

    dependencies {
        classpath("org.testcontainers:postgresql:1.19.3")
    }
}

plugins {
    id("org.springframework.boot") version "3.2.0"
    id("io.spring.dependency-management") version "1.1.4"
    id("org.asciidoctor.jvm.convert") version "3.3.2"
    id("io.gitlab.arturbosch.detekt") version "1.23.3"
    id("org.flywaydb.flyway") version "9.22.3"
    id("nu.studer.jooq") version "8.2.1"
    id("com.google.devtools.ksp") version "1.9.20-1.0.14"
    kotlin("jvm") version "1.9.20"
    kotlin("plugin.spring") version "1.9.20"
    kotlin("plugin.serialization") version "1.9.20"
    jacoco
}

group = "cr.edify"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
}

sourceSets {
    create("integrationTest") {
        compileClasspath += sourceSets.main.get().output + sourceSets.test.get().output
        runtimeClasspath += sourceSets.main.get().output + sourceSets.test.get().output
    }
}

val integrationTestRuntimeOnly: Configuration by configurations.getting {
    extendsFrom(configurations.testRuntimeOnly.get())
}

val integrationTestImplementation: Configuration by configurations.getting {
    extendsFrom(configurations.testImplementation.get())
}

val asciidoctorExt: Configuration by configurations.creating

val snippetsDir by extra { file("build/generated-snippets") }

dependencies {
    implementation(platform("org.testcontainers:testcontainers-bom:1.19.3"))
    implementation(platform("io.awspring.cloud:spring-cloud-aws-dependencies:3.0.3"))
    implementation(platform("io.arrow-kt:arrow-stack:1.2.1"))

    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-jooq")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("io.awspring.cloud:spring-cloud-aws-starter-sqs")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.flywaydb:flyway-core")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("io.github.oshai:kotlin-logging-jvm:5.1.0")
    implementation("net.javacrumbs.shedlock:shedlock-spring:5.10.0")
    implementation("net.javacrumbs.shedlock:shedlock-provider-jooq:5.10.0")
    implementation("com.github.f4b6a3:uuid-creator:5.3.2")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.1")
    implementation("io.arrow-kt:arrow-core")
    implementation("io.arrow-kt:arrow-optics")
    implementation("io.arrow-kt:arrow-optics-reflect")
    implementation("net.pearx.kasechange:kasechange:1.4.1")
    implementation("io.micrometer:micrometer-tracing-bridge-otel")
    implementation("net.ttddyy.observation:datasource-micrometer-spring-boot:1.0.2")

    runtimeOnly("io.micrometer:micrometer-registry-otlp")
    runtimeOnly("io.opentelemetry:opentelemetry-exporter-otlp")
    runtimeOnly("org.postgresql:postgresql")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("io.mockk:mockk-jvm:1.13.8")
    testImplementation("net.datafaker:datafaker:2.0.2")
    testImplementation("org.springframework.boot:spring-boot-devtools")

    integrationTestImplementation("org.springframework.restdocs:spring-restdocs-mockmvc")
    integrationTestImplementation("org.springframework.security:spring-security-test")
    integrationTestImplementation("org.springframework.boot:spring-boot-testcontainers")
    integrationTestImplementation("org.testcontainers:junit-jupiter")
    integrationTestImplementation("org.testcontainers:postgresql")
    integrationTestImplementation("org.testcontainers:localstack")

    jooqGenerator("org.postgresql:postgresql")

    asciidoctorExt("org.springframework.restdocs:spring-restdocs-asciidoctor")

    ksp("io.arrow-kt:arrow-optics-ksp-plugin:1.2.1")
}

val databaseContainer = createDatabaseContainer()

flyway {
    url = databaseContainer.jdbcUrl
    user = databaseContainer.username
    password = databaseContainer.password
}

jooq {
    version.set(dependencyManagement.importedProperties["jooq.version"])
    configurations {
        create("main") {
            jooqConfiguration.apply {
                jdbc.apply {
                    driver = "org.postgresql.Driver"
                    url = databaseContainer.jdbcUrl
                    user = databaseContainer.username
                    password = databaseContainer.password
                }
                generator.apply {
                    name = "org.jooq.codegen.KotlinGenerator"
                    database.apply {
                        name = "org.jooq.meta.postgres.PostgresDatabase"
                        inputSchema = "public"
                        excludes = "flyway_schema_history|shedlock"
                        forcedTypes = listOf(
                            ForcedType()
                                .withUserType("cr.edify.demo.notifications.NotificationStatus")
                                .withConverter("cr.edify.demo.common.jooq.converters.NotificationStatusConverter")
                                .withIncludeTypes("NOTIFICATION_STATUS")
                        )
                    }
                    generate.apply {
                        isDeprecated = false
                        isRecords = true
                        isImmutablePojos = false
                        isFluentSetters = true
                        isKotlinNotNullRecordAttributes = true
                    }
                    target.apply {
                        packageName = "cr.edify.demo.jooq"
                    }
                    strategy.name = "org.jooq.codegen.DefaultGeneratorStrategy"
                }
            }
        }
    }
}

tasks.named<nu.studer.gradle.jooq.JooqGenerate>("generateJooq") {
    // ensure database schema has been prepared by Flyway before generating the jOOQ sources
    dependsOn(tasks.flywayMigrate)

    inputs.dir("src/main/resources/db/migration")
        .withPropertyName("migrations")
        .withPathSensitivity(RELATIVE)

    allInputsDeclared.set(true)

    doLast {
        databaseContainer.stop()
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
    finalizedBy(tasks.jacocoTestReport)
}

tasks.asciidoctor {
    inputs.dir(snippetsDir)
    configurations("asciidoctorExt")
    dependsOn(integrationTest)
}

val integrationTest = task<Test>("integrationTest") {
    description = "Runs integration tests."
    group = "verification"

    minHeapSize = "2G"
    maxHeapSize = "2G"

    testClassesDirs = sourceSets["integrationTest"].output.classesDirs
    classpath = sourceSets["integrationTest"].runtimeClasspath
    shouldRunAfter("test")

    useJUnitPlatform()
    outputs.dir(snippetsDir)

    testLogging {
        events("passed")
    }

    finalizedBy(tasks.jacocoTestReport)
}

tasks.check { dependsOn(integrationTest) }

tasks.withType<io.gitlab.arturbosch.detekt.Detekt>().configureEach {
    reports {
        html.required.set(true)
    }
}

tasks.jacocoTestReport {
    dependsOn(tasks.test, integrationTest)
}

fun createDatabaseContainer(): PostgreSQLContainer<Nothing> {
    val container = PostgreSQLContainer<Nothing>("postgres:15.1")
    container.start()
    return container
}
