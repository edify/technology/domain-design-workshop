# Domain Design Workshop

# Session 1

- Finish modeling our first notification NoGoalsEnrolledNotification it requires 2 events: when the account is created and when the student enrolls a goal (the event types need to be defined)

- Add the logic to manage the notification state

# Session 2

- Refactor the methods `notificationThresholdReached` and `notificationObsolescenceReached` in `NoGoalsEnrollmentNotification` to match the new properties

- There is a possible issue with the `notificationThresholdReached` and `notificationObsolescenceReached` in `NoGoalsEnrollmentNotification` and how they are used in the parent `notify` method

# Session 3

- Add the table creation statement for `NoGoalsEnrollmentNotificationConfiguration`.
- Add any necessary indexes to our initial migration based on natural keys you identify in the 2 tables we have so far.
- Refactor `AccountCreatedEventListener` and extract the notification creation to a service layer.
- Add validation to the `AccountCreatedEventListener` listen method.
- Add the listener for the `GoalEnrollmentEvent` similar to the account created we are not interested in the implementation of the repositories only the logic related to that event.

# Session 4

- Add the rest of the unit tests
- Implement the repositories
- Integrate the listeners with SQS
