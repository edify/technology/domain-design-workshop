#!/bin/bash

localstack_container=$(docker ps | grep localstack/localstack | cut -d' ' -f1)

echo "-----> Cleaning up data..."
PGPASSWORD=demo psql -h postgres -U demo demo -c "DELETE FROM NO_GOALS_ENROLLED_NOTIFICATIONS;"
PGPASSWORD=demo psql -h postgres -U demo demo -c "DELETE FROM NO_GOALS_ENROLLED_NOTIFICATION_CONFIGURATIONS;"

echo "-----> Creating no goals enrolled notification configuration for customer 1..."
PGPASSWORD=demo psql -h postgres -U demo demo -c "INSERT INTO no_goals_enrolled_notification_configurations (id, customer_id, time_between_events) VALUES ('$(uuidgen -t)', 1, '1M');"

echo "-----> Sending account created events for students 1 to 5..."
for studentId in $(seq 1 5); do
	tp=$(otel-cli exec -k producer --tp-print --service Edready --name "account-created-event-publisher" sleep 0.004s | tail -n1 | cut -d= -f2 | tr -d '\n')
	echo "-----> With traceparent $tp"

	docker exec "$localstack_container" awslocal sqs send-message \
		--queue-url http://localstack:4566/000000000000/demo-student-account-created \
		--message-body "{\"occurredAt\": \"$(date -u +"%Y-%m-%dT%H:%M:%SZ")\", \"customerId\": 1, \"studentId\": $studentId}" \
		--message-attributes "{\"traceparent\":{\"StringValue\":\"$tp\",\"DataType\":\"String\"}}"
done

echo "-----> Sleeping for 30s before sending goal enrolled events..."
sleep 30s

echo "Sending goal enrolled events for students 1 to 2..."
for studentId in $(seq 1 2); do
	tp=$(otel-cli exec -k producer --tp-print --service Edready --name "goal-enrollment-event-publisher" sleep 0.004s | tail -n1 | cut -d= -f2 | tr -d '\n')
	echo "-----> With traceparent $tp"

	docker exec "$localstack_container" awslocal sqs send-message \
		--queue-url http://localhost:4566/000000000000/demo-goal-enrolled \
		--message-attributes "{\"traceparent\":{\"StringValue\":\"$tp\",\"DataType\":\"String\"}}" \
		--message-body "{\"occurredAt\": \"$(date -u +"%Y-%m-%dT%H:%M:%SZ")\", \"customerId\": 1, \"studentId\": $studentId}"
done
